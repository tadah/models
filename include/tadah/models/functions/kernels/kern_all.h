#include <tadah/models/functions/kernels/kern_base.h>
#include <tadah/models/functions/kernels/kern_linear.h>
#include <tadah/models/functions/kernels/kern_rbf.h>
#include <tadah/models/functions/kernels/kern_lq.h>
#include <tadah/models/functions/kernels/kern_polynomial.h>
#include <tadah/models/functions/kernels/kern_sigmoid.h>
#include <tadah/models/functions/kernels/kern_quadratic.h>
