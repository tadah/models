#ifndef KERN_QUADRATIC_H
#define KERN_QUADRATIC_H

#include <tadah/models/functions/kernels/kern_base.h>

/**
 * Quadratic kernel - special case of 2nd order polynomial kernel
 *
 * Defined for two vectors **x** and **y**:
 *
 * \f[
 * K(\mathbf{x}, \mathbf{y}) = \Big( \mathbf{x}^T \mathbf{y} \Big)^2
 * = \Big( \sum_i x^{(i)} y^{(i)} \Big)^2
 * \f]
 *
 *  @see Kern_Base
 */
class Kern_Quadratic : public virtual Kern_Base {
    public:
        Kern_Quadratic ();
        Kern_Quadratic (const Config &c);
        /**
         * Label used for this class
         */
        std::string label = "Kern_Quadratic";
        double operator() (const aed_type& b, const aed_type& af) const override;
        aed_type derivative(const aed_type& b, const aed_type& af) const override;
        double prime(const aed_type& b, const aed_type& af, const aed_type& ff) const override;
        std::string get_label() const override;

};
#endif
