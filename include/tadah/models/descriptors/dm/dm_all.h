#include <tadah/models/descriptors/dm/dm_base.h>
#include <tadah/models/descriptors/dm/dm_dummy.h>
#include <tadah/models/descriptors/dm/dm_eam.h>
#include <tadah/models/descriptors/dm/dm_ead.h>
#include <tadah/models/descriptors/dm/dm_blip.h>
#include <tadah/models/descriptors/dm/dm_mEAD.h>
#include <tadah/models/descriptors/dm/dm_mjoin.h>

#include <tadah/models/descriptors/dm/dm_func.h>
